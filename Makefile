# Makefile

APPNAME = cesar

.PHONY: all install clean

all:


install:
	install -d ${DESTDIR}/${BINDIR}
	install -m 0755 ${APPNAME} ${DESTDIR}/${BINDIR}

clean:


